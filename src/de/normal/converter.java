package de.normal;


import de.main.timing;

import java.util.InputMismatchException;
import java.util.Scanner;

public class converter {
    private static int ans;
    public static void convert(int stu, int min, int sek, Scanner x){

        while(sek >= 60){
            sek = sek - 60;
            if(sek < 0){
                System.out.println("Fehler " + "Sekunden" + "zu klein");
                timing.sleep(2*1000);
                System.exit(0);
            }
            min++;
        }

        while(min >= 60){
            min = min - 60;
            if(min < 0){
                System.out.println("Fehler " + "Minuten" + "zu klein");
                timing.sleep(2*1000);
                System.exit(0);
            }
            stu++;
        }
        if(stu >= 24){
            System.out.println("Wollen sie das Programm wirklich laufen lassen? \nDie Dauer wäre min. 1 Tag\n");
            System.out.println("1) Ja");
            System.out.println("2) Nein");
            System.out.println("");
            System.out.println(">");
            try{
                ans = x.nextInt();

            }catch (InputMismatchException e){
                //TODO Fehler zulassen
                e.getStackTrace();
                System.out.println("Falsche Eingabe \nBeende");
                timing.sleep(2*1000);
                System.exit(0);
            }
            if(ans == 1){
                run.lauf(stu, min, sek, x);
            }else{
                System.out.println("Versuche es mit kürzeren Zeiten");
                normal.setTime(x);
            }

        }
        timing.sleep(2*1000);
        run.lauf(stu, min, sek, x);

    }
}

package de.normal;

import de.main.ConsoleInterface;
import de.main.timing;

import java.util.InputMismatchException;
import java.util.Scanner;

public class normal {

    private static int errors = 0;

    private static int setEinzelTime(){
        int ret = 0;
        Scanner x = new Scanner(System.in);

        try {
            ret = x.nextInt();
            errors = 0;
            x.nextLine();
        }catch(InputMismatchException e){
            x.nextLine();
            ret = 0;
            cat(e, x);
        }

        return ret;
    }

    public static void setTime(Scanner x){

        int stunden;
        int minuten;
        int sekunden;

        System.out.println("");
        System.out.print("Stunden : ");
        stunden = setEinzelTime();
        stopLoop(stunden);

        System.out.print("Minuten : ");
        minuten = setEinzelTime();
        stopLoop(minuten);

        System.out.print("Sekunden : ");
        sekunden = setEinzelTime();
        stopLoop(sekunden);

        timing.sleep(1000);
        converter.convert(stunden, minuten, sekunden, x);
    }

    private static void cat(InputMismatchException e, Scanner x){
        int maxtrys = 4;
        if(errors > maxtrys){
            System.out.println("Wiederkehrender Fehler Beende");
            System.exit(0);
        }
        errors++;
        e.getStackTrace();
        System.out.println("[ERROR] - Das war kein Int");
        setTime(x);
    }

    private static void stopLoop(int stop){
        if(stop == 999){
            ConsoleInterface.start();
        }
    }
}

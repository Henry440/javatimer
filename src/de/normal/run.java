package de.normal;

import de.main.ConsoleInterface;
import de.main.timing;

import java.util.InputMismatchException;
import java.util.Scanner;

public class run {
    static int mode = 1;
    static int sleeptime = 990;
    static String ersatzstunde = "", ersatzminute = "", ersatzsekunde = "";
    public static void lauf(int stu, int min, int sek, Scanner x){

        int errors = 0;
        boolean lauf = true;

        System.out.println("Wie soll der countdown aussehen?\n");
        System.out.println("1) hh:mm:ss");
        System.out.println("2) hh:mm");
        System.out.println("3) mm:ss\n");
        System.out.print(">");

        try{
            mode = x.nextInt();
            errors = 0;
        }catch (InputMismatchException e) {
            int maxtrys = 4;
            if (errors > maxtrys) {
                System.out.println("Wiederkehrender Fehler Beende");
                System.exit(0);
            }
            errors++;
            e.getStackTrace();
            System.out.println("[ERROR] - Das war kein String");
        }

        while(lauf){
            //Verzögerung
            timing.sleep(sleeptime);


            //Ersatzzeichen
            if(sek < 10){
                ersatzsekunde = "0";
            }else{
                ersatzsekunde = "";
            }

            if(min < 10){
                ersatzminute = "0";
            }else{
                ersatzminute = "";
            }

            if(stu < 10){
                ersatzstunde = "0";
            }else{
                ersatzstunde = "";
            }

            //Ausgabe
            if(mode == 1){
                System.out.println(ersatzstunde + stu + ":" + ersatzminute + min + ":" + ersatzsekunde + sek);
            }else if(mode == 2){
                System.out.println(ersatzstunde + stu + ":" + ersatzminute + min);
            }else if(mode == 3){
                System.out.println(ersatzminute + min + ":" + ersatzsekunde + sek);
            }else{
                System.out.println(ersatzstunde + stu + ":" + ersatzminute + min + ":" + ersatzsekunde + sek);
            }

            //Ende abfrage
            if(stu == 0 && min == 0 && sek == 0){
                ConsoleInterface.endenormal(x);
            }

            if(sek > 0){
                sek--;
            }else{
                if(min > 0){
                    min--;
                    sek = 59;
                }else{
                    if(stu > 0){
                        stu--;
                        min = 59;
                        sek = 59;
                    }else{
                        beende();
                    }
                }
            }
        }

    }
    private static void beende(){
        System.out.println("Ende");
        timing.sleep(2*1000);
        System.exit(0);
    }
}

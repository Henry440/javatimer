package de.schleife;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SchleifenModus {

    private static int dauer[][];

    private static int stunde;
    private static int minute;
    private static int sekunde;

    static int errors = 0;

    private static int setInt(Scanner s){
        int ret = 0;
        try{
            ret = s.nextInt();
            //s.next();
            errors = 0;
        }catch (InputMismatchException e){
            if(errors > 4){
                System.out.println("Wiederkehrender Fehler");
                s.next();
                s.close();
                System.exit(0);
            }
            System.out.println("Das war kein Int");
            errors++;
            setInt(s);
        }
        return ret;
    }


    public static void lauf(Scanner s){
        System.out.println("Wie viele Schleifen sollen es sein?");
        System.out.print(">");
        int anzahl = setInt(s);
        dauer = new int[anzahl][3];

        for(int i = 0; i < dauer.length; i++){
            for(int j = 0; j < 3; j++){
                if(j == 0){
                    System.out.print("Stunden : ");
                    stunde = setInt(s);
                    dauer[i][j] = stunde;
                }else if(j == 1){
                    System.out.print("Minuten : ");
                    minute = setInt(s);
                    dauer[i][j] = minute;
                }else if(j == 2){
                    System.out.print("Sekunden : ");
                    sekunde = setInt(s);
                    dauer[i][j] = sekunde;
                }
            }
        }

        schleifeLauf.lauf(dauer);
    }
}

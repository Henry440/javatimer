package de.schule;

import de.main.timing;

public class schuleLauf {

    static int lenght;
    static int aktuellDauer;
    static int sleeptime = 990;

    public static void lauf(int[] dauer){
        lenght = dauer.length;
        for(int i = 0; i < lenght; i++){
            aktuellDauer = dauer[i];
            ablauf(aktuellDauer);
        }

    }

    private static void ablauf(int minuten){
        int sekunden = 0;
        boolean aktiv = true;

        while(aktiv){
            //Überprüfen ob Zuende
            if (minuten == 0 && sekunden == 0){
                aktiv = false;
                continue;
            }
            //Pause
            timing.sleep(sleeptime);

            //Zähler
            if(sekunden > 0){
                sekunden--;
            }else{
                if (minuten > 0){
                    minuten--;
                    sekunden = 59;
                }else{
                    aktiv = false;
                    continue;
                }
            }
            print(minuten, sekunden);

        }

    }

    private static void print(int minuten, int sekunden){
        String ermin;
        String ersek;

        if(sekunden < 10){
            ersek = "0";
        }else{
            ersek = "";
        }

        if(minuten < 10){
            ermin = "0";
        }else {
            ermin = "";
        }

        System.out.println(ermin + minuten + ":" + ersek + sekunden);
    }
}

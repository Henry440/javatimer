package de.schleife;

import de.main.timing;

public class schleifeLauf {

    private static int stunde;
    private static int minute;
    private static int sekunde;

    public static void lauf(int zeiten[][]){

        for(int i = 0; i < zeiten.length; i++){

            for(int j = 0; j < 3; j++){
                if(j == 0){
                    stunde = zeiten[i][j];
                }else if(j == 1){
                    minute = zeiten[i][j];
                }else if(j == 2){
                    sekunde = zeiten[i][j];
                }
            }
            while(sekunde > 59){
                sekunde = sekunde - 60;
                minute++;
            }
            while(minute > 59){
                minute = minute - 60;
                stunde++;
            }
            countdown(stunde, minute, sekunde);
        }

    }

    private static void countdown(int stu, int min, int sek){
        boolean aktiv = true;

        while(aktiv){
            if(stu == 0 && min == 0 && sek == 0){
                aktiv = false;
                continue;
            }

            timing.sleep(990);

            if(sek > 0){
                sek--;
            }else{
                if(min > 0){
                    min--;
                    sek = 59;
                }else{
                    if(stu > 0){
                        stu--;
                        min = 59;
                        sek = 59;
                    }else{
                        aktiv = false;
                        continue;
                    }
                }
            }

            print(stu, min, sek);


        }

    }

    private static void print(int stu, int min, int sek){
        String erstu = "";
        String ermin = "";
        String ersek = "";

        if(stu < 10){
            erstu = "0";
        }else{
            erstu = "";
        }

        if(min < 10){
            ermin = "0";
        }else{
            ermin = "";
        }

        if(sek < 10){
            ersek = "0";
        }else{
            ersek = "";
        }

        System.out.println(erstu + stu + ":" + ermin + min + ":" + ersek + sek);

    }

}

package de.main;

import de.normal.normal;
import de.schleife.SchleifenModus;
import de.schule.SchulModus;

import java.util.InputMismatchException;
import java.util.Scanner;


public  class ConsoleInterface {
    private static int errors = 0;
    private static int ans = 0;

    private static int maxtrys = 4;

    public static void start() {
        System.out.println("");
        System.out.println("1) Normalmodus");
        System.out.println("2) Schulmodus");
        System.out.println("3) Schleifenmodus");
        System.out.println("");
        System.out.println("4) Beenden");
        System.out.print(">");

        Scanner s = new Scanner(System.in);
        try {
            int auswahl = s.nextInt();
            errors = 0;

            if (auswahl == 1) {
                s.nextLine();
                normal.setTime(s);
            } else if (auswahl == 2) {
                s.nextLine();
                SchulModus.lauf(s);
            } else if (auswahl == 3) {
                s.nextLine();
                SchleifenModus.lauf(s);
            } else if (auswahl == 4) {
                s.nextLine();
                //TODO Endfunktion schreiben
                System.exit(0);
            } else {
                s.nextLine();
                System.out.println("Keine Option gefunden");
                timing.sleep(5 * 1000);
            }
            start();

        } catch (InputMismatchException e) {
            if (errors > maxtrys) {
                s.nextLine();
                System.out.println("Wiederkehrender Fehler Beende");
                System.exit(0);
                s.close();
            }
            errors++;
            s.nextLine();
            e.getStackTrace();
            System.out.println("[ERROR] - Das war kein String");
            start();
        }


    }

    public static void endenormal(Scanner x){
        System.out.println("Zeit Abgelaufen");
        System.out.println("");
        System.out.println("1) Erneut");
        System.out.println("2) Main Menü");
        System.out.println("");
        System.out.println("3) Beenden");
        System.out.print(">");

        try{
            ans = x.nextInt();
            errors = 0;
        }catch (InputMismatchException e){
            if(errors > maxtrys){
                x.nextLine();
                System.out.println("Wiederkehrender Fehler \nBeende");
                System.exit(0);
                x.close();
            }else{
                errors++;
                x.nextLine();
                System.out.println("[ERROR] - Das war Kein Int");
                endenormal(x);
            }

        }

        if(ans == 1){
            x.nextLine();
            normal.setTime(x);
        }else if(ans == 2){
            x.nextLine();
            start();
        }else {
            System.out.println("Beende das Programm");
            timing.sleep(2*1000);
            System.exit(0);
        }


    }

}

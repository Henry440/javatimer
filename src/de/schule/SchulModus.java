package de.schule;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SchulModus {


    private static String name = "Stunde";
    static int[] dauer;

    static int stunde = 45;
    static int klpause = 5;
    static int grpause = 30;
    static int error = 999;
    static int errors = 0;

    public static void lauf(Scanner s){

        System.out.println("Anzahl der Stunden eingeben");
        System.out.print(">");
        int lauf = 1;

        try{
            lauf = s.nextInt();
            errors = 0;
            s.nextLine();
        }catch (InputMismatchException e){
            keinInt(e, s);
            lauf(s);
        }

        dauer = new int[lauf];

        for(int i = 0; i < lauf; i++){
            String number = "" + i;
            name = name + number;
            setzeZeitInterface();
            int auswahl = 0;

            try{
                auswahl = s.nextInt();
                errors = 0;
                s.nextLine();
            }catch (InputMismatchException e){
                keinInt(e, s);
                auswahl = error;
            }

            setzeZeit(i, auswahl);
        }
        checkArray(s);
    }

    public static void checkArray(Scanner s){
        boolean isTrue = false;
        for(int i = 0; i < dauer.length; i++){
            if(dauer[i] == error){
                int j = i +1;
                System.out.println("Fehler in Stunde " + j);
                setzeZeitInterface();

                int auswahl = 0;

                try{
                    auswahl = s.nextInt();
                    errors = 0;
                    s.nextLine();
                }catch (InputMismatchException e){
                    keinInt(e, s);
                    auswahl = error;
                    checkArray(s);
                }

                setzeZeit(i , auswahl);
                isTrue = true;
            }
        }
        if(isTrue){
            checkArray(s);
        }else{
            schuleLauf.lauf(dauer);
        }
    }

    private static void setzeZeit(int i, int auswahl){
        if(auswahl == 1){
            dauer[i] = stunde;
        }else if(auswahl == 2){
            dauer[i] = klpause;
        }else if(auswahl == 3){
            dauer[i] = grpause;
        }else{
            dauer[i] = error;
        }
    }

    private static void setzeZeitInterface(){
        System.out.println("Was ist es?");
        System.out.println("1) 45 min Stunde");
        System.out.println("2) 5 min Pause");
        System.out.println("3) 30 min Pause");
        System.out.print(">");
    }

    private static void keinInt(Exception e, Scanner s){
        if(errors > 4){
            s.nextLine();
            s.close();
            System.exit(0);
        }
        errors++;
        s.nextLine();
        System.out.println("Das was kein Int");
    }

}
